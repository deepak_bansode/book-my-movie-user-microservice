module github.com/deepakvbansode/book-my-movie-user-microservice

go 1.20

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
)
