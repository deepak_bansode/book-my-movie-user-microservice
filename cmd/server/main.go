package main

import (
	"fmt"
	"net/http"

	userService "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/services"
	userRepo "github.com/deepakvbansode/book-my-movie-user-microservice/internal/driven/in-memory"
	httpServer "github.com/deepakvbansode/book-my-movie-user-microservice/internal/driver/http-server"
)

func main() {
	fmt.Println("You can do it Deepak")

	config, err := httpServer.GetEnvConfig()
	if err != nil {
		panic("No config")
	}

	user := userRepo.NewUser()
	userService.InitUserService(user)
	router := httpServer.NewRouter()
	router.InitializeRouter(config)
	err = http.ListenAndServe(":"+config.Port, router)
	if err != nil {
		panic(err)
	}

}
