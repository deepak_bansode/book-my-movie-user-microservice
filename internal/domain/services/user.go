package services

import (
	"sync"

	entities "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/entities"
	repositories "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/repositories"
)

type UserService interface {
	GetUsers() ([]entities.User, error)
}

var userSvc UserService
var userServiceOnce sync.Once

type userService struct {
	//logger log
	userRepository repositories.UserRepository
}

func InitUserService(userRepository repositories.UserRepository) UserService {
	userServiceOnce.Do(func() {
		userSvc = &userService{userRepository: userRepository}
	})
	return userSvc
}

func GetUserService() UserService {
	if userSvc == nil {
		panic("UserService not initialized")
	}
	return userSvc
}

func (u *userService) GetUsers() ([]entities.User, error) {
	return u.userRepository.GetUsers()
}
