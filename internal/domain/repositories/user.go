package repositories

import (
	entities "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/entities"
)

type UserRepository interface {
	GetUsers() ([]entities.User, error)
	//GetUserByID(ID string) (entities.User, error)
}
