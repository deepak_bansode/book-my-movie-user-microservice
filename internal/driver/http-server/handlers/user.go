package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	userService "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/services"
)

func GetUserHandler(userService userService.UserService) http.HandlerFunc {
	return func(w http.ResponseWriter, request *http.Request) {
		//code to handle
		users, err := userService.GetUsers()
		if err != nil {
			fmt.Println("Failed to get users", users)
			w.WriteHeader(http.StatusInternalServerError)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(users)

	}
}
