package httpserver

const (
	HealthCheckURI     = "/v1/healthcheck"
	HealthCheckAPIName = "HealthCheck"
	GetUsersURI        = "/v1/users"
	GetUsersAPIName    = "GetUsers"
)
