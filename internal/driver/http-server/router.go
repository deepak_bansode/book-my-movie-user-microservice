package httpserver

import (
	"net/http"

	userService "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/services"
	handlers "github.com/deepakvbansode/book-my-movie-user-microservice/internal/driver/http-server/handlers"
	"github.com/gorilla/mux"
)

type Router struct {
	*mux.Router
}

func NewRouter() *Router {
	return &Router{mux.NewRouter()}
}

func (r *Router) InitializeRouter(config *WebServerConfig) {
	s := (*r).PathPrefix(config.RoutePrefix).Subrouter()

	s.HandleFunc(HealthCheckURI, handlers.HealthCheckHandler()).Methods(http.MethodGet).Name(HealthCheckAPIName)
	s.HandleFunc(GetUsersURI, handlers.GetUserHandler(userService.GetUserService())).Methods(http.MethodGet).Name(GetUsersAPIName)
}
