package inmemory

import (
	entities "github.com/deepakvbansode/book-my-movie-user-microservice/internal/domain/entities"
)

type user struct {
}

func NewUser() user {
	return user{}
}

func (u user) GetUsers() ([]entities.User, error) {
	return []entities.User{{ID: "12", Name: "Karan"}}, nil
}
